﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Web;

namespace DynamicForms.Web
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "DynamicService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select DynamicService.svc or DynamicService.svc.cs at the Solution Explorer and start debugging.
    public class DynamicService : IDynamicService
    {
        public IEnumerable<string> LoadPages()
        {
            string appdata = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, @"App_Data");
            IEnumerable<string> results = Directory.EnumerateFiles(appdata, "*.dyn", SearchOption.TopDirectoryOnly);
            return results;
        }
        public string LoadPage(string fileName)
        {
            string appdata = Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, @"App_Data");
            string result = Directory.EnumerateFiles(appdata, fileName, SearchOption.TopDirectoryOnly).ToList().FirstOrDefault();
            string fileContent = File.ReadAllText(result);
            return fileContent;
        }
    };
}
