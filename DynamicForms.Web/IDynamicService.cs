﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace DynamicForms.Web
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDynamicService" in both code and config file together.
    [ServiceContract]
    public interface IDynamicService
    {
        [OperationContract]
        IEnumerable<string> LoadPages();
        [OperationContract]
        string LoadPage(string fileName);
    };
}
