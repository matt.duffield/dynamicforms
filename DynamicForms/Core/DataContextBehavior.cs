﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Interactivity;
using System.Reflection;

namespace Core.UI.Interactivity
{
    /// <summary>
    /// This behavior allows the user to provide a ViewModelLocator and 
    /// property for assigning the DataContext of the object.
    /// Example: <core:DataContextBehavior ViewLocatorName="DynamicForms.ViewModelLocator" DataContextName="TestPage1ViewModel" />
    /// </summary>
    public class DataContextBehavior : Behavior<FrameworkElement>
    {
        #region ViewLocatorName
        /// <summary>
        /// This property the name of the ViewLocator to bind to.
        /// </summary>
        public string ViewLocatorName
        {
            get { return (string)GetValue(ViewLocatorNameProperty); }
            set { SetValue(ViewLocatorNameProperty, value); }
        }
        public static readonly DependencyProperty ViewLocatorNameProperty =
            DependencyProperty.Register("ViewLocatorName",
            typeof(string), typeof(DataContextBehavior), null);

        #endregion

        #region DataContextName
        /// <summary>
        /// This property the name of the DataContext to bind to.
        /// </summary>
        public string DataContextName
        {
            get { return (string)GetValue(DataContextNameProperty); }
            set { SetValue(DataContextNameProperty, value); }
        }
        public static readonly DependencyProperty DataContextNameProperty =
            DependencyProperty.Register("DataContextName",
            typeof(string), typeof(DataContextBehavior), null);

        #endregion

        #region Overrides
        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.Loaded += OnLoaded;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            this.AssociatedObject.Loaded -= OnLoaded;
        }
        #endregion

        #region Events

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            // Override the default DataContext.
            if (ViewLocatorName != null && DataContextName != null)
            {
                Type vt = Type.GetType(ViewLocatorName);
                PropertyInfo pi = vt.GetProperty(DataContextName);
                object context = pi.GetValue(null, null);
                if (context != null)
                {
                    this.AssociatedObject.DataContext = context;
                }
            }
        }

        #endregion
    };
}
