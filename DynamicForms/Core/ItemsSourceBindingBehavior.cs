﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Interactivity;
using System.Reflection;
using System.Linq;
using System.Windows.Data;

namespace Core.UI.Interactivity
{
    /// <summary>
    /// This behavior provides the ability to bind an ItemsSource to an object
    /// that is different from is DataContext.  This is common when you have
    /// controls inside a DataGrid or other collection control that changes
    /// the DataContext for each child object.
    /// Example:  <core:ItemsSourceBindingBehavior ItemsSourceName="Pages" />
    /// </summary>
    public class ItemsSourceBindingBehavior : Behavior<FrameworkElement>
    {
        #region ItemsSourceName
        /// <summary>
        /// The name of the ItemsSource that will be accessed from the DataContext of the root control.
        /// </summary>
        public string ItemsSourceName
        {
            get { return (string)GetValue(ItemsSourceNameProperty); }
            set { SetValue(ItemsSourceNameProperty, value); }
        }
        public static readonly DependencyProperty ItemsSourceNameProperty =
            DependencyProperty.Register("ItemsSourceName", typeof(string),
            typeof(ItemsSourceBindingBehavior), null);

        #endregion
        
        #region BindingMode
        /// <summary>
        /// Describes how data propogates in binding.
        /// </summary>
        public BindingMode BindingMode
        {
            get { return (BindingMode)GetValue(BindingModeProperty); }
            set { SetValue(BindingModeProperty, value); }
        }
        public static readonly DependencyProperty BindingModeProperty =
            DependencyProperty.Register("BindingMode", typeof(BindingMode),
            typeof(ItemsSourceBindingBehavior), new PropertyMetadata(BindingMode.TwoWay));

        #endregion

        #region Overrides
        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.Loaded += OnLoaded;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();

            this.AssociatedObject.Loaded -= OnLoaded;
        }
        #endregion

        #region Events

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(this.ItemsSourceName))
            {
                // Get the root of the UI.
                FrameworkElement element = Utilities.VisualTreeUtility.FindAncestor<UserControl>(this.AssociatedObject);
                if (element != null && element.DataContext != null)
                {
                    PropertyInfo pi = null;
                    object viewModel = element.DataContext;
                    object value = element.DataContext;

                    // The ItemSourceName could possible be a dot notation.  See example below:
                    // <i:Interaction.Behaviors>
                    //     <int:ItemsSourceBehavior ItemsSourceName="CurrentItem.mm_df_DataFields" />
                    // </i:Interaction.Behaviors>

                    string[] properties = this.ItemsSourceName.Split('.');
                    if (properties.Length > 1)
                    {
                        foreach (string p in properties)
                        {
                            // Get the property from the DataContext as defined in the ItemsSourceName property.
                            pi = value.GetType().GetProperty(p, BindingFlags.Public | BindingFlags.Instance);
                            if (pi != null)
                            {
                                // Get the value from the property.
                                value = pi.GetValue(value, null);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                    else
                    {
                        // Get the property from the DataContext as defined in the ItemsSourceName property.
                        pi = value.GetType().GetProperty(this.ItemsSourceName, BindingFlags.Public | BindingFlags.Instance);
                        if (pi != null)
                        {
                            // Get the value from the property.
                            value = pi.GetValue(value, null);
                        }
                    }

                    if (pi != null && value != null)
                    {
                        if (value is System.Collections.IEnumerable)
                        {
                            Binding binding = new Binding(this.ItemsSourceName);
                            binding.Mode = this.BindingMode;
                            binding.Source = viewModel;
                            DependencyProperty prop;

                            prop = ItemsControl.ItemsSourceProperty;
                            AssociatedObject.SetBinding(prop, binding);
                        }
                    }
                }
            }
        }

        #endregion
    };
}
