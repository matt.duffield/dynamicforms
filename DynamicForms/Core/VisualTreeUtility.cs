﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;

namespace Core.Utilities
{
    public class VisualTreeUtility
    {
        public static T RecurseAncestor<T>(object obj) where T : class
        {
            while (obj != null)
            {
                T o = obj as T;
                if (o != null)
                    return o;
                obj = VisualTreeHelper.GetParent(obj as DependencyObject);
            }
            return null;
        }


        public static T FindAncestor<T>(DependencyObject obj) where T : DependencyObject
        {
            while (obj != null)
            {
                T o = obj as T;
                if (o != null)
                    return o;
                obj = VisualTreeHelper.GetParent(obj);
            }
            return null;
        }

        public static T FindAncestor<T>(UIElement obj) where T : UIElement
        {
            return FindAncestor<T>((DependencyObject)obj);
        }

        public static DependencyObject FindAncestorByName(DependencyObject obj, string name)
        {
            while (obj != null)
            {
                FrameworkElement element = obj as FrameworkElement;
                if (element != null && element.Name == name)
                {
                    return element;
                }
                obj = VisualTreeHelper.GetParent(obj);
            }

            return null;
        }

        public static DependencyObject FindAncestor(DependencyObject obj, Func<DependencyObject, bool> predicate)
        {
            if (predicate(obj))
            {
                return obj;
            }

            DependencyObject parent = null;

            FrameworkElement element = obj as FrameworkElement;
            if (element != null)
            {
                parent = element.Parent ?? VisualTreeHelper.GetParent(element);
            }

            if (parent != null)
            {
                return FindAncestor(parent, predicate);
            }

            return null;
        }

        public static T FindChildByName<T>(string name, DependencyObject obj) where T : FrameworkElement
        {
            DependencyObject child = null;
            int count = VisualTreeHelper.GetChildrenCount(obj);
            if (count > 0)
            {
                for (int i = 0; i < count; i++)
                {
                    child = VisualTreeHelper.GetChild(obj, i);
                    T element = child as T;
                    if (element != null && element.Name == name)
                    {
                        break;
                    }
                    else if (child != null)
                    {
                        child = FindChildByName<T>(name, child);
                        if (element != null && element.Name == name)
                        {
                            break;
                        }
                    }
                }
            }

            return child as T;
        }

        public static T FindChild<T>(DependencyObject obj) where T : FrameworkElement
        {
            foreach (DependencyObject child in GetChildren(obj))
            {
                T elem = child as T;
                if (elem != null)
                {
                    return elem;
                }

                elem = FindChild<T>(child);
                if (elem != null)
                {
                    return elem;
                }
            }

            return null;
        }

        private static IEnumerable<DependencyObject> GetChildren(DependencyObject reference)
        {
            int childCount = VisualTreeHelper.GetChildrenCount(reference);
            for (int i = 0; i < childCount; i++)
            {
                yield return VisualTreeHelper.GetChild(reference, i);
            }
        }

        public static T FindChildControl<T>(DependencyObject outerDepObj, string name) where T : DependencyObject
        {
            T child = null;

            for (int index = 0; index < VisualTreeHelper.GetChildrenCount(outerDepObj); index++)
            {
                DependencyObject depObj = VisualTreeHelper.GetChild(outerDepObj, index);

                if (depObj is T)
                {
                    FrameworkElement fe = depObj as FrameworkElement;
                    if (fe != null)
                    {
                        if (fe.Name == name)
                        {
                            child = depObj as T;
                        }

                    }
                }
                else if (VisualTreeHelper.GetChildrenCount(depObj) > 0)
                {
                    child = FindChildControl<T>(depObj, name);
                }

                if (child != null)
                {
                    break;
                }
            }

            return child;
        }
    };
}
