﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Interactivity;
using System.Reflection;

namespace Core.UI.Interactivity
{
    /// <summary>
    /// This trigger looks up the VisualTree from AssociatedObject and finds the
    /// first UserControl and takes its Datacontext.  It then takes
    /// the Method property and invokes it when ever the trigger is fired.
    /// Also, you cannot overload methods or it will throw an execption.
    /// Example: <core:ExecuteDataMethod Method="OnHitMe2" UserState=""/>
    /// </summary>
    public class ExecuteDataMethod : TriggerAction<FrameworkElement>
    {
        private FrameworkElement _element = null;

        #region Method
        /// <summary>
        /// This property the method name to invoke when the trigger is fired.
        /// </summary>
        public string Method
        {
            get { return (string)GetValue(MethodProperty); }
            set { SetValue(MethodProperty, value); }
        }
        public static readonly DependencyProperty MethodProperty =
            DependencyProperty.Register("Method",
            typeof(string), typeof(ExecuteDataMethod), null);

        #endregion

        #region UserState
        /// <summary>
        /// The UserState provides a means to supply state information.
        /// </summary>
        public string UserState
        {
            get { return (string)GetValue(UserStateProperty); }
            set { SetValue(UserStateProperty, value); }
        }
        public static readonly DependencyProperty UserStateProperty =
            DependencyProperty.Register("UserState", typeof(string),
            typeof(ExecuteDataMethod), null);

        #endregion

        #region Overrides
        protected override void Invoke(object parameter)
        {
            // Get the root of the UI.
            if (_element == null)
            {
                _element = Utilities.VisualTreeUtility.FindAncestor<UserControl>(this.AssociatedObject);
                if (_element == null && this.AssociatedObject.DataContext != null)
                    _element = this.AssociatedObject;
            }

            if (_element != null && _element.DataContext != null)
            {
                object bindingTarget = _element.DataContext;

                MethodInfo method = null;
                try
                {
                    method = bindingTarget.GetType().GetMethod(this.Method);
                }
                catch (AmbiguousMatchException ex)
                {
                    throw new InvalidOperationException("Method name cannot be overloaded!", ex);
                }

                if (method != null)
                {
                    ParameterInfo[] parameters = method.GetParameters();
                    if (parameters.Length == 0)
                    {
                        method.Invoke(bindingTarget, null);
                    }
                    else if (parameters.Length == 1)
                    {
                        if (UserState != null &&
                            parameters[0].ParameterType.IsAssignableFrom(this.UserState.GetType()))
                        {
                            method.Invoke(bindingTarget, new object[] { UserState });
                        }
                        else if (this.AssociatedObject != null &&
                                 parameters[0].ParameterType.IsAssignableFrom(this.AssociatedObject.GetType()))
                        {
                            method.Invoke(bindingTarget, new object[] { AssociatedObject });
                        }
                    }
                    else if (parameters.Length == 2 && this.AssociatedObject != null && parameter != null)
                    {
                        if (parameters[0].ParameterType.IsAssignableFrom(this.AssociatedObject.GetType()) &&
                            parameters[1].ParameterType.IsAssignableFrom(parameter.GetType()))
                        {
                            method.Invoke(bindingTarget, new object[] { this.AssociatedObject, parameter });
                        }
                    }
                    else if (parameters.Length == 3 && this.AssociatedObject != null && parameter != null &&
                             this.UserState != null)
                    {
                        if (parameters[0].ParameterType.IsAssignableFrom(this.AssociatedObject.GetType()) &&
                            parameters[1].ParameterType.IsAssignableFrom(parameter.GetType()) &&
                            parameters[2].ParameterType.IsAssignableFrom(this.UserState.GetType()))
                        {
                            method.Invoke(bindingTarget, new object[] { this.AssociatedObject, parameter, UserState });
                        }
                    }
                }
            }
        }
        #endregion
    };
}
