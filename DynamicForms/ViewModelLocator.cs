﻿using Core.Models;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace DynamicForms
{
    public static class ViewModelLocator
    {
        private static TestPage1ViewModel _testPage1ViewModel;
        public static TestPage1ViewModel TestPage1ViewModel {
            get
            {
                if (_testPage1ViewModel == null)
                {
                    _testPage1ViewModel = new TestPage1ViewModel();
                }
                return _testPage1ViewModel;
            }
        }

    };

    public class TestPage1ViewModel : ModelBase
    {

        private string _greeting = "Hello World";
        public string Greeting
        {
            get { return _greeting; }
            set
            {
                _greeting = value;
                OnPropertyChanged(() => Greeting);
            }
        }        

        public void OnHitMe2(string value)
        {
            MessageBox.Show("You hit me too!! I am in my own ViewModel.", "TestPage1ViewModel", MessageBoxButton.OK);
        }

    };
}
