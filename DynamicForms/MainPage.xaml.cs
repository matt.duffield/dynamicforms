﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace DynamicForms
{
    public partial class MainPage : UserControl, INotifyPropertyChanged
    {
        GlobalServices.DynamicServiceClient _client = new GlobalServices.DynamicServiceClient();

        #region ctor
        public MainPage()
        {
            InitializeComponent();

            this.DataContext = this;

            _client = new GlobalServices.DynamicServiceClient();
            _client.LoadPageCompleted += _client_LoadPageCompleted;
            _client.LoadPagesCompleted += _client_LoadPagesCompleted;
            _client.LoadPagesAsync();
        }

        #endregion

        #region Properties

        private string _selectedPage = null;
        public string SelectedPage
        {
            get { return _selectedPage; }
            set
            {
                _selectedPage = value;
                RaisePropertyChanged("SelectedPage");
            }
        }

        private ObservableCollection<string> _pages = new ObservableCollection<string>();
        public ObservableCollection<string> Pages 
        {
            get { return _pages; }
            set
            {
                _pages = value;
                RaisePropertyChanged("Pages");
            }
        }

        private FrameworkElement _dynamicContent = null;
        public FrameworkElement DynamicContent
        {
            get { return _dynamicContent; }
            set
            {
                _dynamicContent = value;
                RaisePropertyChanged("DynamicContent");
            }
        }

        #endregion

        #region Event Handlers

        public void OnLoad(string value)
        {
            if (this.SelectedPage != null)
            {
                _client.LoadPageAsync(this.SelectedPage);
            }
        }

        public void OnHitMe(string value)
        {
            MessageBox.Show("You just hit me!!", "MainPage", MessageBoxButton.OK);
        }

        #endregion

        #region GlobalServices CallBacks

        void _client_LoadPageCompleted(object sender, GlobalServices.LoadPageCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message);
                return;
            }
            this.DynamicContent = XamlReader.Load(e.Result) as FrameworkElement;

        }
        void _client_LoadPagesCompleted(object sender, GlobalServices.LoadPagesCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message);
                return;
            }
            for (int i = 0; i < e.Result.Count; i++)
            {
                string fileName = System.IO.Path.GetFileName(e.Result[i]);
                Pages.Add(fileName);
            }
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;
        public void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    };
}
